//DECLARATIONS
let getLoan = document.getElementById('getLoan');
let depositElement = document.getElementById('bank');
let buyNow = document.getElementById('buy-now');
let computersElement = document.getElementById('computers');
let priceElement = document.getElementById('price')
let imageElement = document.getElementById('image')
let descriptionElement = document.getElementById('description')
let specsForComputer = document.getElementById('specs')
let balanceElement = document.getElementById('balance')
balanceElement.innerText = 0;
let storedBalanceElement = document.getElementById('work')
let sallaryElement = document.getElementById('payment-amount')
sallaryElement.innerText = 0;
let payLoanElement = document.getElementById("repay-loan");
let remainingLoanAmount = document.getElementById("remaining-loan-amount")
let imageSrc = "https://hickory-quilled-actress.glitch.me/"
let computers = [];
let totalDue = 0;
let balance = 0;
let loanAmount = 0;
let sallary = 0;


//fetching of API and laptop functionality
const API_URL = "https://hickory-quilled-actress.glitch.me/computers"
fetch(API_URL)
    .then(response => response.json())
    .then(data => computers = data)
    .then(computers => addComputersToOptions(computers));

//Loops over computers fetched etc
const addComputersToOptions = (computers) => {
    computers.forEach(x => addComputerToOptions(x));
    priceElement.innerHTML = computers[0].price;
    imageElement.innerHTML = imageSrc.concat(computers[0].image);
    descriptionElement.innerHTML = computers[0].description;
}
//Gets title for currently selected computer + ID
const addComputerToOptions = (computer) => {
    const computerElement = document.createElement("option");
    computerElement.value = computer.id;
    computerElement.appendChild(document.createTextNode(computer.title));
    computersElement.appendChild(computerElement);
}
//Gets specs for the currently selected computer
const addSpecsToComputers = e => {
    const computerSpecs = computers[e.target.selectedIndex]
    console.log(computerSpecs.specs);
    specsForComputer.innerText = computerSpecs.specs;
}
//Gets description for the currently selected computer
const addDescriptionToComputers = e => {
    const computerDescription = computers[e.target.selectedIndex];
    descriptionElement.innerText = computerDescription.description;
}
//Gets the price for currently selected computer
const setComputerPrice = e => {
    const selectedComputer = computers[e.target.selectedIndex];
    priceElement.innerText = selectedComputer.price;
}
//Gets the image for currently selected computer
const getImageForComputer = e => {
    const selectedImage = computers[e.target.selectedIndex];
    imageElement.innerText = selectedImage.image;
    imageElement.setAttribute("src", imageSrc.concat(selectedImage.image))
}
//format input to NOK
const formatInputs = (input) => {
    input = Number(new Intl.NumberFormat('no-NO', { style: 'currency', currency: 'NOK' }).format(input));
    return input;
}
//banking functionality
//I initially thought of making a button using JS but I'd have 
//to update the page and restructure a lot of the functionality
//so I decided to just change its visibility in case of a loan being created

//here is the code to make the button either way
/*function makeButton () {
    let repayButton = document.createElement("button", id="repayLoan");
        repayButton.innerHTML = "Repay Loan";
        document.body.appendChild(repayButton);
}*/
const askForLoan = () => {
    //takes input for the loan mount using prompt
    let input = Number(window.prompt("Enter Loan Amount", ""));
    formatInputs(input)
    //if statement here checks if there exists a loan
    if (!loanAmount == 0) {
        throw new Error("You must repay your previous loan before taking a new one")
    }
    //checks if loan is 2x balance
    else if (input <= (balance * 2)) {
        loanAmount += input;
        balance += input;
        //changing the text displayed
        balanceElement.innerText = balance;
        remainingLoanAmount.innerText = input
        //change button visibility
        document.getElementById("repay-loan").style.display = 'block'
        document.getElementById("remaining-loan").style.display = 'block';
        document.getElementById("remaining-loan-amount").style.display = 'block';
        return loanAmount;
    }
    else {
        console.log(input);
        throw new Error("The loan you have asked for is larger than 2x your balance")
    }
}
const deposit = () => {
    //checks if loan exists and transferes 10% to loan and 90% to balance
    if (loanAmount > 0) {
        loanAmount -= sallary * 0.1
        balance += sallary * 0.9;
    }
    if (loanAmount === 0) {
        balance += sallary;
        sallary -= sallary;
        balanceElement.innerText = balance
        sallaryElement.innerText = sallary
    }
    return balance;
}
function doWork() {
    //adds 100 NOK to storedBalance
    sallary += 100;
    sallaryElement.innerText = sallary
    console.log(sallary)
    return sallary;
}
function payLoan() {
    //logic to determine loan downpayment etc
    if (sallary > loanAmount) {
        balance += sallary - loanAmount;
        loanAmount = 0;
        sallary = 0;
        balanceElement.innerText = balance;
        sallaryElement.innerText = 0;
        //changing visibility of elements when loan is repayed
        document.getElementById("remaining-loan").style.display = 'none';
        document.getElementById("remaining-loan-amount").style.display = 'none';
    } else if (sallary < loanAmount) {
        loanAmount -= sallary;
        sallaryElement.innerText = 0;
        //change remaining loan
    } else if (sallary == 0) {
        throw new Error('You have not worked enough yet.. lazy bastard')
    }
}

function buyPc () {
    //buys a PC if balance is sufficient
    if (balance > priceElement.innerText) {
        balance -= priceElement.innerText
        balanceElement.innerText = balance
        alert('Congratulations! you just bought this computer')
        console.log('Congratulations! you just bought this computer');
    } else {
        alert('You cannot afford that computer');
        console.log('You cannot afford that computer');
    }
}
//Eventlisteners for different buttons and selections.
getLoan.addEventListener('click', askForLoan)
depositElement.addEventListener('click', deposit)
payLoanElement.addEventListener('click', payLoan)
buyNow.addEventListener('click', buyPc)
storedBalanceElement.addEventListener('click', doWork)
computersElement.addEventListener('change', addSpecsToComputers)
computersElement.addEventListener('change', setComputerPrice)
computersElement.addEventListener('change', getImageForComputer)
computersElement.addEventListener('change', addDescriptionToComputers)